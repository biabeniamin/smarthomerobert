#include "Sensors.h"

Sensors::Sensors(char temperaturePin)
{
	_temperaturePin = temperaturePin;


}

void Sensors::Setup()
{
	oneWire = new OneWire(_temperaturePin);
	DS18B20 = new DallasTemperature(oneWire);

	//start listening channel for temperature
	DS18B20->begin();
}

void Sensors::UpdateTemperature()
{
	float tempC;

	DS18B20->requestTemperatures();
	tempC = DS18B20->getTempCByIndex(0);

	_temperature = tempC;

	//DS18B20->requestTemperatures();
	//_temperature = DS18B20->getTempCByIndex(0);
	//dtostrf(tempC, 2, 2, temperatureCString);
}

float Sensors::GetTemperature()
{
	return _temperature;
}