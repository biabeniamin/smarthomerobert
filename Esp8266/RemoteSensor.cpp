#include "RemoteSensor.h"

RemoteSensor::RemoteSensor()
{
}


unsigned long RemoteSensor::GetLastTrigger()
{
	return _lastMotion;
}

void RemoteSensor::SensorTriggered()
{
	//check if motion was detected now
	_lastMotion = millis();
}

unsigned int RemoteSensor::TotalSecondsFromLastUpdate()
{
	//this values are in miliseconds, convert to seconds
	return (millis() - _lastMotion) / 1000;
}

unsigned int RemoteSensor::SecondsFromLastUpdate()
{
	//get seconds in a minute, to display time with minutes and seconds. Values is not bigger than 60

	unsigned int seconds = TotalSecondsFromLastUpdate();

	return seconds % 60;

}

unsigned int RemoteSensor::MinutesFromLastUpdate()
{
	//get totals minutes

	unsigned int seconds = TotalSecondsFromLastUpdate();

	return seconds / 60;
}

String RemoteSensor::GetTimeAsString()
{
	String text = "";

	text = String(MinutesFromLastUpdate());
	text += " minutes and ";

	text += String(SecondsFromLastUpdate());
	text += "seconds ago";

	return text;
}


