#pragma once

#include <Arduino.h>
#include "RemoteSensor.h"

enum CARD_TYPE_ENUM
{
	NO_RFID_CARD_DETECTED = 0,
	UNKNOWN_RFID_CARD_DETECTED = 1,
	KNOWN_RFID_CARD_DETECTED = 2
};

class Rfid : public RemoteSensor
{
public:
	Rfid();

	char* GetMessage();

	void KnownCardDetected();
	void UnknownCardDetected();

private:
	CARD_TYPE_ENUM _cardType;
	char _message[100];
};