#include <ESP8266WiFi.h>
#include "Webserver.h"
#include "HtmlBuilder.h"
#include "Pir.h"
#include "Flame.h"
#include "Notifications.h"
#include "Water.h"

#include "Sensors.h"
#include "LanCommunication.h"
#include <SoftwareSerial.h>

SoftwareSerial swSer(12, 14, false, 256);

void writeLan(int byte)
{
  swSer.write(byte);
}
int readLan()
{
  return swSer.read();
}
int countLan()
{
  return swSer.available();
}

LanCommunication lanCom(&writeLan, &readLan, &countLan);


Pir pir1;
Pir pir2;
Pir pir3;
Flame flame;
Rfid rfid;
Water water;
Sensors sensors(13);
Notifications notif;

Webserver server;
HtmlBuilder htmlBuilder(&pir1, &pir2, &pir3, &flame, &rfid, &water, &sensors, &notif);


#define TEMPERATURE_REFRESH_TIME 1000
unsigned long lastTemperatureUpdate = 0;

void setup() {
  Serial.begin(9600);
  swSer.begin(9600);
  delay(10);

  server.Start(&htmlBuilder);
  sensors.Setup();

}

void loop() {

  if (TEMPERATURE_REFRESH_TIME < (millis() - lastTemperatureUpdate))
  {
    float temperature;

    sensors.UpdateTemperature();
    temperature = sensors.GetTemperature();

    lastTemperatureUpdate = millis();
  }

  if (lanCom.ReadCommand())
  {
    int *command = lanCom.GetLastCommand();
    for (int i = 0; i < 4; i++)
    {
      Serial.print(command[i]);
      Serial.print(" ");
    }
    Serial.println(" comm");

    switch (command[0])
    {
      //rfid card detected
      case 0:
        switch (command[1])
        {
          case 0:
            rfid.UnknownCardDetected();
            notif.UnknownCardDetected();
            break;
          case 1:
            rfid.KnownCardDetected();
            break;
        }
        break;
      //in case the motion was detected
      case 1:
        switch (command[1])
        {
          //pir 3 triggered
          case 1:
            pir1.MotionDetected();
            notif.MotionDetected();
            break;
          //pir 3 triggered
          case 2:
            pir2.MotionDetected();
            notif.MotionDetected();
            break;
          //pir 3 triggered
          case 3:
            pir3.MotionDetected();
            notif.MotionDetected();
            break;
        }
        break;
      //in case the temperature was transmitted
      case 2:
        flame.FireDetected();
        notif.FireDetected();

        break;
      case 3:
        water.WaterDetected();
        notif.WaterDetected();

        break;
    }
  }

  server.CheckForConnectedClients();

}

