#pragma once

#include <Arduino.h>
#include "RemoteSensor.h"

class Pir : public RemoteSensor
{
public:
	Pir();

	char* GetMessage();

	void MotionDetected();

private:
	char _message[100];
};