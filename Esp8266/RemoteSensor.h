#pragma once

#include <Arduino.h>

class RemoteSensor
{
public:
	RemoteSensor();

	void SensorTriggered();
	unsigned long GetLastTrigger();

private:
	unsigned long _lastMotion;
	char _message[100];

protected:
	unsigned int TotalSecondsFromLastUpdate();
	unsigned int SecondsFromLastUpdate();
	unsigned int MinutesFromLastUpdate();
	String GetTimeAsString();

};