#include "Rfid.h"

Rfid::Rfid()
{
}


char* Rfid::GetMessage()
{
	String text;

	text = "";
	if (0 == GetLastTrigger())
	{
		return "No card was detected";
	}
	

	if (UNKNOWN_RFID_CARD_DETECTED == _cardType)
	{
		text = "Unknown card was detected ";
	}
	else if (KNOWN_RFID_CARD_DETECTED == _cardType)
	{
		text = "Known card was detected ";
	}

	text += GetTimeAsString();

	text.toCharArray(_message, 100);

	return _message;
}

void Rfid::KnownCardDetected()
{
	//check if motion was detected now
	SensorTriggered();
	_cardType = KNOWN_RFID_CARD_DETECTED;
}

void Rfid::UnknownCardDetected()
{
	SensorTriggered();
	_cardType = UNKNOWN_RFID_CARD_DETECTED;
}
