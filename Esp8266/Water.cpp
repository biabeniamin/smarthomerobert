#include "Water.h"

Water::Water()
{
}


char* Water::GetMessage()
{
	String text;

	text = "";
	if (0 == GetLastTrigger())
	{
		return "No water was detected";
	}
	
	text = "Water was detected ";

	text += GetTimeAsString();

	text.toCharArray(_message, 100);

	return _message;
}

void Water::WaterDetected()
{
	//check if motion was detected now
	SensorTriggered();
}
