#include "Pir.h"

Pir::Pir()
{
}


char* Pir::GetMessage()
{
	String text;

	text = "";
	if (0 == GetLastTrigger())
	{
		return "No motion was detected";
	}
	
	text = "Motion was detected ";

	text += GetTimeAsString();

	text.toCharArray(_message, 100);

	return _message;
}

void Pir::MotionDetected()
{
	//check if motion was detected now
	SensorTriggered();
}
