#include "Flame.h"

Flame::Flame()
{
}


char* Flame::GetMessage()
{
	String text;

	text = "";
	if (0 == GetLastTrigger())
	{
		return "No fire was detected";
	}
	
	text = "Fire was detected ";

	text += GetTimeAsString();

	text.toCharArray(_message, 100);

	return _message;
}

void Flame::FireDetected()
{
	//check if motion was detected now
	SensorTriggered();
}
