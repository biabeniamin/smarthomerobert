#pragma once

#include <Arduino.h>

class Notifications
{
public:
	Notifications();

	void MotionDetected();
	void UnknownCardDetected();
	void FireDetected();
	void WaterDetected();

	int GetNotification();

private:
	int _notificationId = 0;
};