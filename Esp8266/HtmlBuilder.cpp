#include "HtmlBuilder.h"


HtmlBuilder::HtmlBuilder(Pir *pirRoom1, Pir *pirRoom2, Pir *pirRoom3, Flame* flame, Rfid* rfid, Water* water, Sensors* sensors, Notifications *notif)
{
	_pirRoom1 = pirRoom1;
	_pirRoom2 = pirRoom2;
	_pirRoom3 = pirRoom3;
	_flame = flame;
	_rfid = rfid;
	_water = water;
	_sensors = sensors;
	_notif = notif;
}

char* HtmlBuilder::GetHtml()
{
	String html = defaultHtmlPage;

	//pir 1
	html += _pirRoom1->GetMessage();
	//end line
	html += " in room 1<br>";

	//pir 2
	html += _pirRoom2->GetMessage();
	//end line
	html += " in room 2<br>";

	//pir 3
	html += _pirRoom3->GetMessage();
	//end line
	html += " in room 3<br>";

	//flame
	html += _flame->GetMessage();
	//end line
	html += "<br>";

	//rfid 
	html += _rfid->GetMessage();
	//end line
	html += "<br>";

	html += "Temperature is ";
	html += String(_sensors->GetTemperature());
	//end line
	html += " degrees celsius<br>";

	//water
	html += _water->GetMessage();
	//end line
	html += "<br>";

	html += defaultEndHtmlPage;

	html.toCharArray(_buffer, 1000);

	return _buffer;
}

char* HtmlBuilder::GetNotificationHtml()
{
	String html = String(_notif->GetNotification());
	Serial.println("Html content:");
	Serial.println(String(html));

	html.toCharArray(_buffer, 500);

	return _buffer;
}
