#pragma once

#include <Arduino.h>
#include "Pir.h"
#include "Flame.h"
#include "Rfid.h"
#include "Water.h"
#include "Sensors.h"
#include "Notifications.h"

class HtmlBuilder
{
public:
	HtmlBuilder(Pir*, Pir*, Pir*, Flame*, Rfid*, Water*, Sensors*, Notifications*);

	char* GetHtml();
	char* GetNotificationHtml();

private:
	char *defaultHtmlPage = "<!DOCTYPE HTML>\r\n<html><head><meta http-equiv='refresh' content='5' / ></head><body bgcolor='#011f33'><center><div style='color:white; background-color: #1b6a9e; width:80%;font-size:20px;'><h2>Smart home</h2>";
	char *defaultEndHtmlPage = "</div></body></center></html>";

	char _buffer[1000];

	Pir *_pirRoom1;
	Pir *_pirRoom2;
	Pir *_pirRoom3;

	Flame *_flame;
	Rfid *_rfid;
	Water *_water;

	Sensors *_sensors;

	Notifications *_notif;

};
