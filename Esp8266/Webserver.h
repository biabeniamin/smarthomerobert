#include <ESP8266WiFi.h>
#include "HtmlBuilder.h"

class Webserver
{
public:
	Webserver();

	void Start(HtmlBuilder*);

	void CheckForConnectedClients();

private:
	const char* ssid = "Bia";
	const char* password = "";

	HtmlBuilder *_htmlBuilder;

	WiFiServer *server;

	void MakeHtmlResponse();
};
