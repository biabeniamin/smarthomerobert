#include "Webserver.h"


Webserver::Webserver()
{
	server = new WiFiServer(80);
}

void Webserver::Start(HtmlBuilder *htmlBuilder)
{
	_htmlBuilder = htmlBuilder;
	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.mode(WIFI_AP);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.println("WiFi connected");

	// Start the server
	server->begin();
	Serial.println("Server started");

	// Print the IP address
	Serial.println(WiFi.localIP());
}

void Webserver::CheckForConnectedClients()
{

	// Check if a client has connected
	WiFiClient client = server->available();
	if (!client) {
		return;
	}

	// Wait until the client sends some data
	Serial.println("new client");
	while (!client.available()) {
		delay(1);
	}

	// Read the first line of the request
	String req = client.readStringUntil('\r');
	Serial.println(req);
	client.flush();

	// Prepare the response
	String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";

	// Match the request
	int val;
	if (req.indexOf("/notif") != -1)
	{
		s += _htmlBuilder->GetNotificationHtml();
		Serial.println("Notification");
	}
	else
	{
		s += _htmlBuilder->GetHtml();
		Serial.println("Normal request");
	}

	client.flush();

	// Send the response to the client
	client.print(s);
	delay(1);
	Serial.println("Client disonnected");

	// The client will actually be disconnected 
	// when the function returns and 'client' object is detroyed
}