#pragma once

#include <Arduino.h>
#include "RemoteSensor.h"

class Water : public RemoteSensor
{
public:
	Water();

	char* GetMessage();

	void WaterDetected();

private:
	char _message[100];
};