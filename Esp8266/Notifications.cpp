#include "Notifications.h"

Notifications::Notifications()
{
}


void Notifications::MotionDetected()
{
	_notificationId = 6;
}

void Notifications::UnknownCardDetected()
{
	_notificationId = 7;
}

void Notifications::FireDetected()
{
	_notificationId = 8;
}
void Notifications::WaterDetected()
{
	_notificationId = 9;
}

int Notifications::GetNotification()
{
	int notificationId;

	notificationId = _notificationId;

	//reset notifications
	_notificationId = 0;


	return notificationId;
}