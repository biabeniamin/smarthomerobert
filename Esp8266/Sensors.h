#pragma once

#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>

class Sensors
{
public:
	Sensors(char);

	void Setup();
	void UpdateTemperature();
	float GetTemperature();

private:
	char _temperaturePin;

	float _temperature;

	OneWire *oneWire;
	DallasTemperature *DS18B20;
};