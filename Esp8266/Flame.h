#pragma once

#include <Arduino.h>
#include "RemoteSensor.h"

class Flame : public RemoteSensor
{
public:
	Flame();

	char* GetMessage();

	void FireDetected();

private:
	char _message[100];
};