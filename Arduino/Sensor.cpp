#include "Sensor.h"

Sensor::Sensor()
{
	
}

Sensor::Sensor(void(*onValueChanged)(int), int id, TRIGGER_TYPE triggerType)
	: Sensor(onValueChanged, id, triggerType, 0, 0)
{
	
}

Sensor::Sensor(void(*onValueChanged)(int), int id, TRIGGER_TYPE triggerType, byte isDebouncingActivated, unsigned long debouncingInterval)
{
	_onValueChanged = onValueChanged;
	_id = id;
	_triggerType = triggerType;
	_isDebouncingActivated = isDebouncingActivated;
	_debouncingInterval = debouncingInterval;
}

int Sensor::GetIntValue()
{
	return _intValue;
}

byte Sensor::GetByteValue()
{
	return _byteValue;
}

float Sensor::GetFloatValue()
{
	return _floatValue;
}

void Sensor::SetIntValue(int value)
{
	_intValue = value;
}

void Sensor::SetByteValue(byte value)
{
	if (value != _byteValue)
	{
		if ((0 != _onValueChanged))
		{
			//either _isDebouncingActivated is deactivated, either _isDebouncingActivated is activated and check interval
			if (!_isDebouncingActivated || 0 == _lastActivation || (_isDebouncingActivated &&_debouncingInterval < (millis() - _lastActivation)))
			{
				if ((TRIGGER_LOW_TO_HIGH == _triggerType) && (0 != value))
				{
					_onValueChanged(_id);
				}
				else if ((TRIGGER_HIGH_TO_LOW == _triggerType) && (0 == value))
				{
					_onValueChanged(_id);
				}

				_lastActivation = millis();
			}
		}

		_byteValue = value;
	}
}

void Sensor::SetFloatValue(float value)
{
	_floatValue = value;
}
