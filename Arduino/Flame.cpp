#include "Flame.h"

Flame::Flame(char pin)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

Flame::Flame(char pin, void(*onMotionDetected)(int), int id)
	: Sensor(onMotionDetected, id, TRIGGER_HIGH_TO_LOW, 1, 10000)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

char Flame::GetValue()
{
	return digitalRead(_pin);
}

void Flame::Check()
{
	//check if motion was detected now
	char motion;

	motion = GetValue();
	
	SetByteValue(motion);
}



