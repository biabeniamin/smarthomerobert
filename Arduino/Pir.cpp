#include "Pir.h"

Pir::Pir(char pin)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

Pir::Pir(char pin, void(*onMotionDetected)(int), int id)
	: Sensor(onMotionDetected, id, TRIGGER_LOW_TO_HIGH)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

char Pir::GetValue()
{
	return digitalRead(_pin);
}

void Pir::Check()
{
	//check if motion was detected now
	char motion;

	motion = GetValue();
	
	SetByteValue(motion);
}



