#pragma once

#include <Arduino.h>

enum TRIGGER_TYPE
{
	TRIGGER_LOW_TO_HIGH,
	TRIGGER_HIGH_TO_LOW,
	TRIGGER_BOTH,
};


class Sensor
{
public:
	Sensor();
	Sensor(void(*onValueChanged)(int), int id, TRIGGER_TYPE);
	Sensor(void(*onValueChanged)(int), int id, TRIGGER_TYPE, byte, unsigned long);

	int GetIntValue();
	byte GetByteValue();
	float GetFloatValue();

protected:
	void SetIntValue(int);
	void SetByteValue(byte);
	void SetFloatValue(float);

private:
	int _intValue;
	byte _byteValue;
	float _floatValue;

	byte _isDebouncingActivated;
	unsigned long _lastActivation;
	unsigned long _debouncingInterval;

	int _id;

	void(*_onValueChanged)(int);

	TRIGGER_TYPE _triggerType;
};