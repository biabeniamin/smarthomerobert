#include "Sensors.h"

Sensors::Sensors(char humidityPin)
{
	_humidityPin = humidityPin;

	
}

void Sensors::Setup()
{
}

void Sensors::Update()
{
	_humidity = analogRead(_humidityPin);
}

int Sensors::GetHumidity()
{
	return _humidity;
}