#include "Pir.h"
#include "Flame.h"
#include "Rfid.h"
#include "PanicButton.h"
#include <SoftwareSerial.h>
#include "LanCommunication.h"
#include "Alarm.h"
#include "Sensors.h"
#include "Appliances.h"

#define HUMIDITY_THRESHOLD 500
#define HUMIDITY_INTERVAL 10000
unsigned long lastHumidityAlert = 0;

void OnMotionDetected(int id);
void OnFlameDetected(int id);

Pir pir1(4, &OnMotionDetected, 1);
Pir pir2(3, &OnMotionDetected, 2);
Pir pir3(5, &OnMotionDetected, 3);
Flame flame(A5, &OnFlameDetected, 0);
PanicButton panicButton(8, &OnPanicButtonPressed, 0);
Sensors sensors(A0);//water detector
Appliances appliances(A4, A3);//light pin, door pin

Rfid rfid(9, 10); //reset and ss
Alarm alarm(2);//buzzer pin

SoftwareSerial mySerial(6, 7);


void writeLan(int byte)
{
  mySerial.write(byte);
}
int readLan()
{
  return mySerial.read();
}
int countLan()
{
  return mySerial.available();
}

LanCommunication lanCom(&writeLan, &readLan, &countLan);

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  Serial.println("start");
  delay(10);
  rfid.Setup();

}

void OnMotionDetected(int id)
{
  alarm.Start();
  Serial.println("Misca");
  Serial.println(id);

  int data[COMMUNICATION_BYTE_COUNT] = { 1, id, 0, 0};
  lanCom.SendCommand(data);
}

void OnFlameDetected(int id)
{
  alarm.Start();
  Serial.println("Flacara");
  Serial.println(id);

  int data[COMMUNICATION_BYTE_COUNT] = { 2, id, 0, 0};
  lanCom.SendCommand(data);
}

void OnPanicButtonPressed(int id)
{
  alarm.Start();
  Serial.println("Panic button");
  Serial.println(id);

}

void loop()
{
  
  sensors.Update();
  if (HUMIDITY_THRESHOLD < sensors.GetHumidity()
    && HUMIDITY_INTERVAL < (millis() - lastHumidityAlert))
  {
    Serial.println(sensors.GetHumidity());
    alarm.Start();

    int data[COMMUNICATION_BYTE_COUNT] = { 3, 0, 0, 0};
    lanCom.SendCommand(data);

    lastHumidityAlert = millis();
  }

  pir1.Check();
  pir2.Check();
  pir3.Check();
  flame.Check();
  panicButton.Check();

  if (lanCom.ReadCommand())
  {
    int *command = lanCom.GetLastCommand();
  }

  CARD_TYPE_ENUM card;

  
  card = rfid.GetCard();
  
  if (KNOWN_RFID_CARD_DETECTED == card)
  {
    alarm.Stop();
    int data[COMMUNICATION_BYTE_COUNT] = { 0, 1, 0, 0};
    lanCom.SendCommand(data);
    Serial.println("Known card");
    appliances.UnlockDoor();
  }
  else if (UNKNOWN_RFID_CARD_DETECTED == card)
  {
    int data[COMMUNICATION_BYTE_COUNT] = { 0, 0, 0, 0};
    lanCom.SendCommand(data);
    Serial.println("unknown card");
  }
}

