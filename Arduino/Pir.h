#pragma once

#include <Arduino.h>
#include "Sensor.h"

class Pir : public Sensor
{
public:
	Pir(char);
	Pir(char pin, void(*onMotionDetected)(int), int id);

	void Check();

private:
	char _pin;

	char GetValue();

};