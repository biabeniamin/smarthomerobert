#pragma once

#include <Arduino.h>

class Appliances
{
public:
	Appliances(char, char);

	void TurnOnLight();
	void TurnOffLight();
	
	void UnlockDoor();
	void LockDoor();


private:
	char _doorPin;
	char _lightPin;
};