#include "Appliances.h"

Appliances::Appliances(char lightPin, char doorPin)
{
	_lightPin = lightPin;
	_doorPin = doorPin;

	pinMode(_lightPin, OUTPUT);
	pinMode(_doorPin, OUTPUT);

	LockDoor();
}

void Appliances::TurnOnLight()
{
	digitalWrite(_lightPin, HIGH);
}
void Appliances::TurnOffLight()
{
	digitalWrite(_lightPin, LOW);
}

void Appliances::UnlockDoor()
{
	digitalWrite(_doorPin, LOW);
	delay(2000);
	LockDoor();
}

void Appliances::LockDoor()
{
	digitalWrite(_doorPin, HIGH);
}