#include "Rfid.h"

Rfid::Rfid(char rst, char ss)
{
	_rst = rst;
	_ss = ss;
}

void Rfid::Setup()
{
	_rfid = new MFRC522(_ss, _rst);

	SPI.begin(); // Init SPI bus
	_rfid->PCD_Init(); // Init MFRC522

	delay(10);

}

void Rfid::Reset()
{
	_rfid = new MFRC522(_ss, _rst);

	SPI.begin(); // Init SPI bus
	_rfid->PCD_Init(); // Init MFRC522

	Serial.println("Reset rfid");
}

CARD_TYPE_ENUM Rfid::GetCard()
{
	CARD_TYPE_ENUM card;
	unsigned long rfidScanTime;

	rfidScanTime = millis();
	card = UNKNOWN_RFID_CARD_DETECTED;

	// Look for new cards
	if (!_rfid->PICC_IsNewCardPresent())
	{
		card = NO_RFID_CARD_DETECTED;
	}

	else
	{
		// Verify if the NUID has been readed
		if (!_rfid->PICC_ReadCardSerial())
			card = NO_RFID_CARD_DETECTED;
		else
		{

			//get type of rfid card
			Serial.print(F("PICC type: "));
			MFRC522::PICC_Type piccType = _rfid->PICC_GetType(_rfid->uid.sak);
			Serial.println(_rfid->PICC_GetTypeName(piccType));

			// Check is the PICC of Classic MIFARE type
			if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
				piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
				piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
				Serial.println(F("Your tag is not of type MIFARE Classic."));
				card = NO_RFID_CARD_DETECTED;
			}
			else
			{
				card = UNKNOWN_RFID_CARD_DETECTED;


				// Store NUID into nuidPICC array
				for (byte i = 0; i < 4; i++)
				{
					nuidPICC[i] = _rfid->uid.uidByte[i];
				}

				Serial.println(F("The NUID tag is:"));

				for (byte i = 0; i < _rfid->uid.size; i++) {
					Serial.print(_rfid->uid.uidByte[i], HEX);
				}

				Serial.println();
				Serial.println();


				// Halt PICC
				_rfid->PICC_HaltA();

				// Stop encryption on PCD
				_rfid->PCD_StopCrypto1();

				for (byte i = 0; i < _rfid_known_cards_count; i++)
				{
					if (CompareTwoCards(_knownCards[i], _rfid->uid.uidByte))
					{
						card = KNOWN_RFID_CARD_DETECTED;
					}
				}

			}
		}
	}

	if (100 < (millis() - rfidScanTime))
	{
		Reset();
	}

	return card;
}

byte Rfid::CompareTwoCards(byte* card1, byte* card2)
{
	for (int i = 0; i < 4; i++)
	{
		if (card1[i] != card2[i])
		{
			return 0;
		}
	}

	return 1;
}