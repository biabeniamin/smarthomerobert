#pragma once

#include <Arduino.h>

class Sensors
{
public:
	Sensors(char);

	void Setup();
	void Update();
	int GetHumidity();

private:
	char _humidityPin;

	int _humidity;

};