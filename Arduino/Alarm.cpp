#include "Alarm.h"

Alarm::Alarm(char pin)
{
	_pin = pin;
}

void Alarm::Start()
{
	//start sound with a frequency of 100 hertz
	tone(_pin, 100);
}

void Alarm::Stop()
{
	//start sound with a frequency of 100 hertz
	noTone(_pin);
}