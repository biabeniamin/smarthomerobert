#pragma once

#include <Arduino.h>

#include <SPI.h>
#include <MFRC522.h>

enum CARD_TYPE_ENUM
{
	NO_RFID_CARD_DETECTED = 0,
	UNKNOWN_RFID_CARD_DETECTED = 1,
	KNOWN_RFID_CARD_DETECTED = 2
};


class Rfid
{
public:
	Rfid(char, char);

	void Setup();

	CARD_TYPE_ENUM GetCard();

	void Reset();

private:

	char _rst;
	char _ss;

	MFRC522 * _rfid;

	MFRC522::MIFARE_Key *_key;

	// Init array that will store new NUID
	byte nuidPICC[4];

	byte _rfid_known_cards_count = 1;
	byte _knownCards[1][4] = { {0x45, 0xC8, 0XA0, 0X2C} };

	byte CompareTwoCards(byte*, byte*);
};