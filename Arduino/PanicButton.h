#pragma once

#include <Arduino.h>
#include "Sensor.h"

class PanicButton : public Sensor
{
public:
	PanicButton(char);
	PanicButton(char pin, void(*onMotionDetected)(int), int id);

	void Check();

private:
	char _pin;

	byte GetValue();

};