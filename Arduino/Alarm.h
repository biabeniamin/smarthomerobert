#pragma once

#include <Arduino.h>

class Alarm
{
public:
	Alarm(char);

	void Start();
	void Stop();


private:
	char _pin;
};