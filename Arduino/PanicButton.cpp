#include "PanicButton.h"

PanicButton::PanicButton(char pin)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

PanicButton::PanicButton(char pin, void(*onMotionDetected)(int), int id)
	: Sensor(onMotionDetected, id, TRIGGER_HIGH_TO_LOW, 1, 10000)
{
	_pin = pin;
	pinMode(_pin, INPUT_PULLUP);
}

byte PanicButton::GetValue()
{
	return digitalRead(_pin);
}

void PanicButton::Check()
{
	//check if motion was detected now
	byte motion;

	motion = GetValue();
	
	SetByteValue(motion);
}



