#pragma once

#include <Arduino.h>
#include "Sensor.h"

class Flame : public Sensor
{
public:
	Flame(char);
	Flame(char pin, void(*onMotionDetected)(int), int id);

	void Check();

private:
	char _pin;

	char GetValue();

};